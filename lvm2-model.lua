local mymodule = {}

-- Load libraries
modelfunctions = require("modelfunctions")
fs = require("acf.fs")
format = require("acf.format")

-- Set variables
local processname = "lvm"
local packagename = "lvm2"

-- ################################################################################
-- LOCAL FUNCTIONS

-- ################################################################################
-- PUBLIC FUNCTIONS

function mymodule.get_startstop(self, clientdata)
        return modelfunctions.get_startstop(processname)
end

function mymodule.startstop_service(self, startstop, action)
        return modelfunctions.startstop_service(startstop, action)
end

function mymodule.getstatus()
	return modelfunctions.getstatus(processname, packagename, "LVM Status")
end

function mymodule.getstatusdetails()
	local status = {}
	status.pvdisplay = cfe({ type="longtext", label="Physical Volumes" })
	status.pvdisplay.value, status.pvdisplay.errtxt = modelfunctions.run_executable({"pvdisplay"})

	status.lvdisplay = cfe({ type="longtext", label="Logical Volumes" })
	status.lvdisplay.value, status.lvdisplay.errtxt = modelfunctions.run_executable({"lvdisplay"})

	return cfe({ type="group", value=status, label="LVM Status Details" })
end

return mymodule
